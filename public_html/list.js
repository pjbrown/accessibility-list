(function()
{
    function scrollTo(object, ms)
    {
        $("html, body").stop();
        if (object.offset())
        {
           var height = object.offset().top - ($(window).height() / 2);
           $("html, body").animate({
                duration: ms,
                scrollTop: height
           });
        }
    }
    window.scrollTo = scrollTo;
    
    var fontSize = 1.0;
    function incrementFontSize(value)
    {
        fontSize += value;
        fontSize = Math.max(0.5, Math.min(fontSize, 2.0));
        $("ol").css("font-size", fontSize + "em");
    }
    window.incrementFontSize = incrementFontSize;
}());

$(function()
{
    $("a.jump").each(function()
    {
        $(this).attr("name", $(this).attr("href"));
        $(this).attr("href", "");
    });
    $("a.jump").on("click", function()
    {
        var target = $($(this).attr("name"));
        scrollTo(target, 800);
        target.focus();
        return false;
    });
    
    $("a.top").attr("href", "");
    $("a.top").on("click", function()
    {
        $("html, body").stop();
        $("html, body").animate({
            duration: 800,
            scrollTop: 0
        });
        $("#top").focus();
        return false;
    });
    
    $("a").not("a.button").focus(function()
    {
        var focus = $(document.activeElement);
        scrollTo(focus, 400);
        return false;
    });
    
    $("h1").focus(function()
    {
        var focus = $(document.activeElement);
        scrollTo(focus, 400);
        return false;
    });
});