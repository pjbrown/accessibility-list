<?php
include "../src/filter_query.php";

// all of this is probably super temporary
header("Content-Type: application/json");
echo '{"list":[';
$delim = "";
foreach ($p->getLinks() as $href => $label)
{
    echo $delim . '{"href":' . json_encode($href) . ',"label":' . json_encode($label) . '}';
    $delim = ",";
}
echo "]}";