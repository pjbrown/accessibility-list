<?php include "../src/filter_query.php"; ?>
<!DOCTYPE html>

<html>
    <head>
        <title>Accessibility List</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:700">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="list.js"></script>
    </head>
    <body>
<?php   if (!$p): ?>
        <form>
            <div class="special">
                <input id="url" type="text" name="url" title="Type a URL here" placeholder="Type a URL here or drag in a bookmark" value="<?=$saved?>">
                <label>Sort by:
                    <select name="sort">
                        <option value="0" <?=$sort == 0 ? "selected" : ""?>>Page Order</option>
                        <option value="1" <?=$sort == 1 ? "selected" : ""?>>Alphabetical Order</option>
                        <option value="2" <?=$sort == 2 ? "selected" : ""?>>Name Length</option>
                    </select>
                </label>
            </div>
            <label><input type="checkbox" name="desc" value="1" <?=$desc ? "checked" : ""?>>Sort Descending</label>
            <label><input type="checkbox" name="external" value="1" <?=$external ? "checked" : ""?>>Allow external domains</label>
            <label><input type="checkbox" name="social" value="1" <?=$social ? "checked" : ""?>>Allow social media</label>
            <input type="submit" action="index.php" value="Go">
        </form>
<?php   else: ?>
        <div class="header">
            <a class="button" href="index.php?saved=<?=$url?>&sort=<?=$sort?>&desc=<?=$desc?>&social=<?=$social?>">Back to Search</a>
            <div class="divider"></div>
            <a class="button" href="javascript:incrementFontSize(-0.25)">Decrease Text Size</a>
            <a class="button" href="javascript:incrementFontSize(0.25)">Increase Text Size</a>
        </div>
<?php   if (!$p->getErrors()): ?>
        <ol>
            <h1 id="top" tabindex="0">Top</h1>
<?php       for ($i = 0; $i < ceil(count($p->getLinks()) / 20); $i++): ?>
            <a class="highlight jump" href="#<?=$i * 20 + 1?>">Links <span class="number"><?=$i * 20 + 1?>-<?=$i * 20 + 20?></span></a>
<?php       endfor; ?>
<?php       $index = 0;
            foreach ($p->getLinks() as $href => $label):
            if ($index % 20 == 0):
            if ($index != 0): ?>
            <a class="highlight top" href="#top">Back to Top</a>
<?php       endif; ?>
            <h1 id="<?=$index + 1?>" tabindex="0"><?=$index + 1?>-<?=$index + 20?></h1>
<?php       endif; ?>
            <a href="<?=$href?>"><li><?=$label?></li></a>
<?php       $index++;
            endforeach;?>
            <a class="highlight top" href="#top">Back to Top</a>
            <a class="highlight back" href="index.php?saved=<?=$url?>&sort=<?=$sort?>&desc=<?=$desc?>&social=<?=$social?>">Back to Search</a>
        </ol>
<?php   else: ?>
        <div class="content">
            <p tabindex="0"><?=$p->getErrors();?></p>
        </div>
<?php   endif;
        endif; ?>
    </body>
</html>