<?php
include "BlacklistInterface.php";
/**
 * JSONBlacklist
 * @access  public
 */
class JSONBlacklist implements BlacklistInterface
{
    private $blacklist;
    
    /**
     * Populates blacklist with entries from a JSON file
     * @param   string  $filename   JSON file to populate blacklist from
     * @access  public
     */
    public function __construct($filename)
    {
        $file = file_get_contents($filename);
        $this->blacklist = json_decode($file);
    }
    
    /**
     * Determines if a given domain is blacklisted
     * @param   string  $category   Blacklist category (e.g. "general", "social")
     * @param   string  $domain     Domain to test if blacklisted
     * @return  bool    Whether the domain is blacklisted or not
     * @access  public
     */
    public function isBlacklisted($category, $domain)
    {
        $patterns = $this->blacklist->{$category};
        
        foreach ($patterns as $match)
        {
            if (strpos($domain, $match) > -1)
            {
                return true;
            }
        }
        
        return false;
    }
}
