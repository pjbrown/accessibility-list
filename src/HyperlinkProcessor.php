<?php
include "JSONBlacklist.php";
/**
 * HyperlinkProcessor
 * @access  public
 */
class HyperlinkProcessor
{
    private $parsedUrl;
    private $dom;
    private $links;
    private $blacklist;
    private $allowExternal;
    private $allowSocial;
    private $errors;
    
    /**
     * Constructor
     * @param   string  $pageUrl        Page to process
     * @param   string  $blacklistFile  File to load blacklist from
     * @access  public
     */
    public function __construct($pageUrl, $blacklistFile, $sort, $desc, $external, $social)
    {
        $this->parsedUrl = parse_url($pageUrl);
        $this->allowExternal = $external;
        $this->allowSocial = $social;
        
        $this->blacklist = new JSONBlacklist($blacklistFile);
        $this->links = array();
        
        $this->errors = false;
        
        if (!empty($pageUrl) && filter_var($pageUrl, FILTER_VALIDATE_URL))
        {
            $this->parseDOM($pageUrl);
            if (!$this->errors)
            {
                $this->populateHyperlinkList($this->dom);
                $this->sortResults($sort, $desc);
                if (empty($this->links))
                {
                    $this->errors = "No links found";
                }
            }
        }
    }
    
    /**
     * Reads a page into a DOM tree
     * @param   string  $filename   Page to retrieve DOM tree from
     * @access  private
     */
    private function parseDOM($filename)
    {
        $curl = curl_init($filename);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        
        $output = curl_exec($curl);
        
        if (curl_errno($curl))
        {
            $this->errors .= curl_error($curl);
        }
        else
        {
            libxml_use_internal_errors(true);
            $this->dom = new DOMDocument();
            $this->dom->loadHTML($output);
        }
        
        curl_close($curl);
    }
    
    /**
     * Recursively populate list of hyperlinks in DOM tree
     * @param   DOMNode $root
     */
    private function populateHyperlinkList($root)
    {
        if ($root->nodeName == "a")
        {
            $label = $this->correctValue($root->nodeValue);
            $parsed = $this->correctHref($root->getAttribute("href"));
            $href = $this->rebuildUrl($parsed);
            
            if ($this->hyperlinkAllowed($label, $href, $parsed))
            {
                $this->links[$href] = $label;
            }
        }
        
        foreach ($root->childNodes as $node)
        {
            if ($node->hasChildNodes())
            {
                $this->populateHyperlinkList($node);
            }
        }
    }
    
    /**
     * Corrects a node value for display by stripping out trailing spaces etc
     * @param   string  $value
     * @return  string
     */
    private function correctValue($value)
    {
        $value = filter_var($value, FILTER_SANITIZE_STRING);
        $value = trim($value);
        $value = str_replace("\n", " ", $value);
        $value = preg_replace("/\ {2,}/", " ", $value);
        $value = ucfirst($value);
        return $value;
    }
    
    /**
     * Corrects a hyperlink for display on the page (e.g. hyperlinks referring to same domain)
     * @param   string  $url    Hyperlink to correct
     * @access  private
     */
    private function correctHref($url)
    {
        $parsed = parse_url(filter_var($url, FILTER_SANITIZE_URL));

        if (!isset($parsed["scheme"]))
        {
            $parsed["scheme"] = $this->parsedUrl["scheme"];
            
            if (!isset($parsed["host"]))
            {
                $parsed["host"] = $this->parsedUrl["host"];
            }
            
            if (isset($this->parsedUrl["path"]))
            {
                if (isset($parsed["path"]) && substr($url, 0, 1) != "/")
                {
                    $parsed["path"] = preg_replace("/[^\/]+$/", "", $this->parsedUrl["path"]) . $parsed["path"];
                }
                else if (!isset($parsed["path"]) && (isset($parsed["query"]) || isset($parsed["fragment"])))
                {
                    $parsed["path"] = $this->parsedUrl["path"];
                }
            }
        }
        else
        {
            if (!isset($parsed["host"]))
            {
                $parsed["host"] = "";
            }
        }
        
        return $parsed;
    }
    
    /**
     * Rebuilds a parsed URL. (Ignores port, user, pass)
     * @param   array   $parsed     A parsed URL array
     */
    private function rebuildUrl($parsed)
    {
        $href = $parsed["scheme"] . "://" . $parsed["host"];
        
        if (isset($parsed["path"]))
        {
            $href .= $parsed["path"];
        }
        if (isset($parsed["query"]))
        {
            $href .= "?" . $parsed["query"];
        }
        if (isset($parsed["fragment"]))
        {
            $href .= "#" . $parsed["fragment"];
        }
        
        return $href;
    }
    
    /**
     * Determines if a particular hyperlink will be allowed to display in the final list
     * @param   string  $value
     * @param   string  $href
     * @return  boolean
     */
    private function hyperlinkAllowed($value, $href, $parsed)
    {
        return !(isset($this->links[$href]) && mb_strlen($this->links[$href]) < mb_strlen($href))
            && !empty($value)
            && !ctype_space($href)
            && $parsed["scheme"] != "javascript"
            && (!(!$this->allowExternal && strpos($href, $this->parsedUrl["host"]) == false) || ($this->allowSocial && $this->blacklist->isBlacklisted("social", $href)))
            && !(!$this->allowSocial && $this->blacklist->isBlacklisted("social", $href));
    }
    
    /**
     * Sorts from shortest string to longest string
     * @param   string  $a
     * @param   string  $b
     * @return  integer
     */
    private function sortByLength($a, $b)
    {
        return mb_strlen($a) == mb_strlen($b) ? 0 : mb_strlen($a) > mb_strlen($b) ? 1 : -1;
    }
    
    /**
     * Sorts from longest string to shortest string
     * @param   string  $a
     * @param   string  $b
     * @return  integer
     */
    private function sortByLengthDesc($a, $b)
    {
        return mb_strlen($a) == mb_strlen($b) ? 0 : mb_strlen($a) < mb_strlen($b) ? 1 : -1;
    }
    
    /**
     * Sorts the list of hyperlinks by some criteria
     * @param   string  $sort
     * @param   bool    $desc
     */
    private function sortResults($sort, $desc)
    {
        switch ($sort)
        {
            // Page Order, only applicable to descending order
            case 0:
                !$desc ?: $this->links = array_reverse($this->links);
                break;
            // Alphabetical Order
            case 1:
                $desc ? arsort($this->links) : asort($this->links);
                break;
            // Length Order
            case 2:
                $desc ? uasort($this->links, [$this, "sortByLengthDesc"]) : uasort($this->links, [$this, "sortByLength"]);
                break;
        }
    }
    
    /**
     * Outputs list of links from processed page
     * @access  public
     * @return  array
     */
    public function getLinks()
    {
        return $this->links;
    }
    
    /**
     * Returns either a boolean (false) indicating no errors or a string with errors.
     * @access  public
     * @return  mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }
}