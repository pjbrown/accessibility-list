<?php
include "../src/HyperlinkProcessor.php";

$url = filter_input(INPUT_GET, "url", FILTER_SANITIZE_URL);
$saved = filter_input(INPUT_GET, "saved", FILTER_SANITIZE_URL);
$sort = filter_input(INPUT_GET, "sort", FILTER_SANITIZE_NUMBER_INT);
$desc = filter_input(INPUT_GET, "desc", FILTER_VALIDATE_BOOLEAN);
$external = filter_input(INPUT_GET, "external", FILTER_VALIDATE_BOOLEAN);
$social = filter_input(INPUT_GET, "social", FILTER_VALIDATE_BOOLEAN);

$p = false;
if (!empty($url))
{
    $p = new HyperlinkProcessor($url, "../config/blacklist.json", $sort, $desc, $external, $social);
}