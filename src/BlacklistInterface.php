<?php
/**
 * BlacklistInterface
 */
interface BlacklistInterface
{
    /**
     * Determines if a given domain is blacklisted
     * @param   string  $category   Blacklist category (e.g. "general", "social")
     * @param   string  $domain     Domain to test if blacklisted
     * @return  bool    Whether the domain is blacklisted or not
     * @access  public
     */
    public function isBlacklisted($category, $domain);
}
