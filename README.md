# Accessibility List #

Processes a page and lists all of its hyperlinks. Some hyperlinks are filtered out to reduce the number of links listed.

Feed a URL into the start page and sorting criteria and whether to allow external domains and/or social media links:

![Start Page](https://pjbrown.bitbucket.io/projects/accessibility/images/accessibility_1.png)

The page will be scraped for everything except its barest hyperlinks, removing any duplicate links and undesired links.

![Accessibility List](https://pjbrown.bitbucket.io/projects/accessibility/images/accessibility_2.png)

### Requirements ###

* PHP 5.6 (might work on older PHP versions, I wouldn't know)
* PHP DOM Extension
* `mbstring` Extension
* Note: Uses jQuery from Google APIs and Lato font from Google Fonts

### Getting Started ###

It's PHP. Copy the files onto a server running PHP or your own local Apache/nginx folder. Don't alter the folder structure; try to ensure that everything in `public_html` can access the class files located in the `src` folder and the config files in the `config` folder, and that the `src` and `config` folders are not accessible to end-users.

The `index.php` client will display a form and extract links from a page for display in a user-friendly and accessible list. The `filter.php` web API will output the hyperlinks extracted from a page in JSON format instead.

The query string is: `/index.php?url=url&sort=0&external=0&social=0`

* **url** sets the page that will be processed for hyperlinks.
* **sort** will alter how the hyperlinks are sorted:
    * 0 to display in the order the hyperlinks appear in the page source
    * 1 to display in alphabetical order
    * 2 to display in order decided by length of hyperlink label
* **external** when set to 1 will allow hyperlinks that refer to external domains, otherwise they are removed from the results
* **social** when set to 1 will allow hyperlinks that refer to social media (Facebook, Twitter etc), otherwise they are removed from the results

This same API is applicable to `filter.php`.

### Why doesn't SSL/TLS work / how to fix "SSL certificate problem: unable to get local issuer certificate" error ###

By default, Windows PHP bundles don't include CA root certificate bundles necessary for certifying sites that are using TLS. Do not try to disable SSL verification with the CURLOPT_SSL_VERIFYPEER flag. The proper way to solve this problem is to download a CA root certificate bundle and point PHP on how to use it.

You can download a CA root certificate bundle (`cacert.pem`) directly from the cURL website: <http://curl.haxx.se/docs/caextract.html> [[Direct Download]](http://curl.haxx.se/ca/cacert.pem)

Place `cacert.pem` somewhere the PHP runtime can find it and add the following line in `php.ini`:

    curl.cainfo="<path to cacert.pem>"